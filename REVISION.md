# 21/3/2021

### ecd1a7597c6eeaac418f9ef1ea78f7b0797d5bb9

---

## Flaws

* Структура каталогів неочевидна. Директорія `src` повинна містити файли для розробки, наприклад `index.ts`, для стилів краще окремий каталог `assets`

### File [package.json](package.json)

* `npx` використовується для запуску команд, якщо відповідний пакет не встановлено локально. В проекті є `package.json` значить треба встановити локальні пакети. Команду `compile` можна переписати `"compile": "tsc"`. Причина: `npx` завантажує останню версію `typescript`, для проекту може бути потрібна зовсім інша версію, тому треба використовувати як є в `package.json`

### File [index.html](dist/index.html)

* Завеликі горизонтальні відступи. Краще 2 пробіли для вкладених тегів. Між сусідніми елементами на одному рівні краще ставити пусту строку, тоді візуально помітні окремі блоки.
  ```
  <div class="calculator__main-panel">
    <canvas id="canvas"></canvas>

    <div class="calculator__control-panel">
      <div class="calculator__info-panel">
        <h2>Info:</h2>
      </div>
    </div>
  </div>
  ```
* ~14-15 Немає необхідності в контейнерах. `.container` і `.calculator` не мають корисного навантаження


### File [index.ts](src/index.ts)

* Вся логіка в одному файлі. Варто використати [модулі](https://www.typescriptlang.org/docs/handbook/modules.html)
* ~50. Для `event.offsetX` та `event.offsetY` вже створені змінні `pointCoordinateX`, `pointCoordinateY`, варто їх використати
* Метод `isCorrectAmountOfPoints` можна оптимізувати:
  * в `case` писати зразу `return ...`, тоді `break` не знадобиться
  * написавши enum, треба їх використовувати. В кожному case має бути enum а не строка:
    ```typescript
    function isCorrectAmountOfPoints(status: StatusAction): boolean {
      switch (StatusAction[status]) {
        case StatusAction.NotEnoughPoints:
          return points.length >= 3;
        case StatusAction.LimitPoints:
          return points.length < 100;
      }
    }
    ```
* Code-style. Перед `return` завжди пуста строка
* ~140 продуктивність коду при таких перевірках значно знижується. Тільки 1 випадок для всіх вершин підійде з умовою `i === points.length - 1`, проте перевірка виконується в кожному циклі. Краще такий особливий випадок обрахувати окремо і виключити з циклу
* ~153 складніше ніж могло би бути. Проходити 2 рази по масивах невиправдано, кінцеве значення `finalResult` можна додавати прямо в циклі і позбутись `arrLinesLength`.
* ~122 функція calculateResult не варто використовувати return для того щоб просто виходити з функції, було б добре якби була якась логіка при якій return щось все-таки повертає, або можна забрати його в цьому випадку та поставити else.

### File [style.css](assets/css/style.css)

* ~14 Проект повинен бути адаптивним. Вказувати фіксовану ширину погана практика, по-можливості треба уникати. Краще орієнтуватись на ширину екрану і від неї відштовхуватись.
* ~29 Конфлікт получився. `.calculator__control-panel` має 25% ширини... плаваючого батьківського елемента `.calculator__main-panel` з `display: flex`. Проблема в тому, що стилі для 25% перераховуються при зміні ширини екрану, а код `index.ts` ~26 не перерахується і получиться невідповідність. З іншої сторони підрахунок в `index.ts` ~26: від ширини екрану забираємо ширину `controlPanel` і 200px. Не розумію для чого 200px

### Extra

* Можна спробувати написати аналогічну сторінку використовуючи Angular. Це дозволить розширити логіку роботи програми, простіше взаємодіяти з кнопками і підвищити надійність роботи.

# 25/3/2021

### 8ca77494a8aa413cc1ad0ac1dbea64c8bc4c6b90

---

## Flaws

* в різних файлах повторюється пошук dom-елементів через `querySelector` або `getElementById`. Пошук dom-вузлів має здійснюватись 1 раз
* немає необхідності в двох кнопках `Draw Figure` і `Calculate`. Одна кнопка може відповідати за створення фігури і обрахунок розмірів
* контроль композиції (layout) html-шаблону: видимість елементів, розміщення на сторінці і розміри, повинен, по-можливості, здійснюватись стилями. Так простіше і менш затратно по ресурсах браузера. Взаєморозміщення малюнка і інфо-зони можна координувати так:
  * файл `windowResize`, видалити `controlPanel` і `controlPanelSize`
  * файл `style.css` 
    * для `.calculator__main-panel` видалити `flex-wrap`, `justify-content`, `align-items`
    * для `.calculator__control-panel` дописати `flex: 1 1 auto;`
    * дописати нове правило
      ```css
      @media screen and (min-width: 551px) {
        .calculator__control-panel {
          padding-left: 10px;
        }
      }

      @media screen and (max-width: 550px) {
        .calculator__main-panel {
          flex-direction: column;
        }
      }
      ```
* популярні системи створення макетів: [Material Design](https://material.io/design/layout/responsive-layout-grid.html#breakpoints) або [Bootstrap](https://getbootstrap.com/docs/5.0/layout/breakpoints/#available-breakpoints) не передбачають перехідного стилю при ширині 550px. Варто змінити на один з найближчих варіантів: 480px || 576px

### File [index.ts](src/index.ts)

* `calculateResult` може передаватись безпосередньо в `addEventListener`, по аналогії з функціями вище

### File [drawing.ts](src/scripts/drawing.ts)

* експортувати допускається константи примітивних типів, функції, класи, інтерфейси. Експорт об'єкта технічно можливий але може привести до непередбачуваної поведінки, помилок і складного пошуку причин неправильної роботи
* ~19 `return` можна видалити, не впливає ніяк на процес
* ~60 нераціональний спосіб очищення масива
