import { showMessage } from './warnings.js';
import { StatusAction } from './statusAction.js';
import { isCorrectAmountOfPoints } from './point.js';
import { calculatePerimeter, calculateSquare } from "./calculate.js";
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var perimeterResult = document.querySelector('.calculator_perimeter-result');
var squareResult = document.querySelector('.calculator_square-result');
var btnDrawCalc = document.querySelector('.btn-draw-calc');
var drawnFigure = false;
var points = [];
export function drawPoint(event) {
    if (!isCorrectAmountOfPoints(StatusAction.LimitPoints, points)) {
        showMessage(StatusAction.LimitPoints);
    }
    else if (!drawnFigure) {
        var pointCoordinateX = event.offsetX;
        var pointCoordinateY = event.offsetY;
        var point = {
            x: pointCoordinateX,
            y: pointCoordinateY
        };
        points.push(point);
        ctx.beginPath();
        ctx.arc(pointCoordinateX, pointCoordinateY, 2, 0, 2 * Math.PI);
        ctx.fill();
        ctx.closePath();
    }
}
export function drawFigure() {
    if (!isCorrectAmountOfPoints(StatusAction.NotEnoughPoints, points)) {
        showMessage(StatusAction.NotEnoughPoints);
    }
    else {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();
        points.forEach(function (point, index) {
            if (index === 0) {
                ctx.moveTo(point.x, point.y);
            }
            ctx.lineTo(point.x, point.y);
        });
        ctx.closePath();
        ctx.fill();
        calculateResult();
        btnDrawCalc.disabled = true;
        drawnFigure = true;
    }
}
export function cleanPanel() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    points.length = 0;
    perimeterResult.textContent = '0';
    squareResult.textContent = '0';
    drawnFigure = false;
    btnDrawCalc.disabled = false;
}
function calculateResult() {
    perimeterResult.textContent = String(calculatePerimeter(points));
    squareResult.textContent = String(calculateSquare(points));
}
