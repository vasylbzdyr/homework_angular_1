export function isCorrectAmountOfPoints(status, points) {
    var amount = points.length;
    var result = true;
    switch (status) {
        case StatusAction.NotEnoughPoints:
            return result = amount >= 3;
        case StatusAction.LimitPoints:
            return result = amount < 100;
    }
}
export function showMessage(status) {
    var statusCalculator = document.querySelector('.calculator__status-text');
    switch (status) {
        case StatusAction.NotEnoughPoints:
            statusCalculator.textContent = 'You need to choose 3 points';
            break;
        case StatusAction.LimitPoints:
            statusCalculator.textContent = 'You can choose only 100 points';
            break;
    }
    statusCalculator.classList.add('warning');
    setTimeout(function () {
        statusCalculator.textContent = "";
        statusCalculator.classList.remove('warning');
    }, 4000);
}
export var StatusAction;
(function (StatusAction) {
    StatusAction[StatusAction["NotEnoughPoints"] = 0] = "NotEnoughPoints";
    StatusAction[StatusAction["LimitPoints"] = 1] = "LimitPoints";
})(StatusAction || (StatusAction = {}));
