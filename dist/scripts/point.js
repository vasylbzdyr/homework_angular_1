import { StatusAction } from './statusAction.js';
export function isCorrectAmountOfPoints(status, points) {
    var amount = points.length;
    var result = true;
    switch (status) {
        case StatusAction.NotEnoughPoints:
            return result = amount >= 3;
        case StatusAction.LimitPoints:
            return result = amount < 100;
    }
}
