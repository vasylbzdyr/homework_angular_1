export var StatusAction;
(function (StatusAction) {
    StatusAction[StatusAction["NotEnoughPoints"] = 0] = "NotEnoughPoints";
    StatusAction[StatusAction["LimitPoints"] = 1] = "LimitPoints";
})(StatusAction || (StatusAction = {}));
