import { StatusAction } from './statusAction.js';
export function showMessage(status) {
    var statusCalculator = document.querySelector('.calculator__status-text');
    switch (status) {
        case StatusAction.NotEnoughPoints:
            statusCalculator.textContent = 'You need to choose 3 points';
            break;
        case StatusAction.LimitPoints:
            statusCalculator.textContent = 'You can choose only 100 points';
            break;
    }
    statusCalculator.classList.add('warning');
    setTimeout(function () {
        statusCalculator.textContent = "";
        statusCalculator.classList.remove('warning');
    }, 4000);
}
