var canvas = document.getElementById('canvas');
var mainPanel = document.querySelector('.calculator__main-panel');
export function windowResize() {
    var canvasSize;
    if (innerWidth <= 550) {
        canvasSize = mainPanel.offsetWidth;
        canvas.height = innerHeight * 0.5;
    }
    else {
        canvasSize = mainPanel.offsetWidth * 0.6;
        canvas.height = innerHeight * 0.8;
    }
    canvas.width = canvasSize;
}
