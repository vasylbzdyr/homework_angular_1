import {windowResize} from './scripts/windowResize.js';
import {cleanPanel, drawFigure, drawPoint} from './scripts/drawing.js';

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const btnDrawCalc = document.querySelector('.btn-draw-calc') as HTMLButtonElement;
const btnClean = document.querySelector('.btn-clean') as HTMLButtonElement;

windowResize();

window.addEventListener('resize', windowResize);
canvas.addEventListener('click', drawPoint);
btnDrawCalc.addEventListener('click', drawFigure);
btnClean.addEventListener('click', cleanPanel);
