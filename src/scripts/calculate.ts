export function calculatePerimeter(points: any[]): number {
    let finalResult = 0;
    let coordinatesX;
    let coordinatesY;

    coordinatesX = Math.abs(points[points.length - 1].x - points[0].x);
    coordinatesY = Math.abs(points[points.length - 1].y - points[0].y);

    finalResult += calcFormula(coordinatesX, coordinatesY);

    for (let i = 0; i < points.length - 1; i++) {
        coordinatesX = Math.abs(points[i + 1].x - points[i].x);
        coordinatesY = Math.abs(points[i + 1].y - points[i].y);

        finalResult += calcFormula(coordinatesX, coordinatesY);
    }

    function calcFormula(coordinatesX: number, coordinatesY: number): number {
        return Math.sqrt(Math.pow(coordinatesX, 2) + Math.pow(coordinatesY, 2));
    }

    return roundNumber(finalResult);
}

export function calculateSquare(points: any[]): number {
    let tempResult: number = 0;
    let tempResult2: number = 0;
    let finalResult: number;

    for (let i = 0; i < points.length - 1; i++) {
        tempResult += points[i].x * points[i + 1].y;
    }

    for (let i = 0; i < points.length - 1; i++) {
        tempResult2 += points[i].y * points[i + 1].x;
    }

    finalResult = 0.5 * Math.abs(tempResult - tempResult2);

    return roundNumber(finalResult);
}

function roundNumber(number: number): number {
    return +number.toFixed(2);
}
