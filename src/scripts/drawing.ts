import {showMessage} from './warnings.js';
import {StatusAction} from './statusAction.js';
import {isCorrectAmountOfPoints, PointType} from './point.js';
import {calculatePerimeter, calculateSquare} from "./calculate.js";

const canvas = document.getElementById('canvas') as HTMLCanvasElement;
const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
const perimeterResult = document.querySelector('.calculator_perimeter-result') as HTMLSpanElement;
const squareResult = document.querySelector('.calculator_square-result') as HTMLSpanElement;
const btnDrawCalc = document.querySelector('.btn-draw-calc') as HTMLButtonElement;

let drawnFigure: boolean = false;
const points: PointType[] = [];

export function drawPoint(event: MouseEvent): void {
    if (!isCorrectAmountOfPoints(StatusAction.LimitPoints, points)) {
        showMessage(StatusAction.LimitPoints);
    } else if (!drawnFigure) {
        const pointCoordinateX: number = event.offsetX;
        const pointCoordinateY: number = event.offsetY;

        const point: PointType = {
            x: pointCoordinateX,
            y: pointCoordinateY
        };

        points.push(point);

        ctx.beginPath();
        ctx.arc(pointCoordinateX, pointCoordinateY, 2, 0, 2 * Math.PI);
        ctx.fill();
        ctx.closePath();
    }
}

export function drawFigure(): void {
    if (!isCorrectAmountOfPoints(StatusAction.NotEnoughPoints, points)) {
        showMessage(StatusAction.NotEnoughPoints);
    } else {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.beginPath();
        points.forEach((point, index) => {
            if (index === 0) {
                ctx.moveTo(point.x, point.y);
            }
            ctx.lineTo(point.x, point.y);
        });
        ctx.closePath();
        ctx.fill();

        calculateResult();
        btnDrawCalc.disabled = true;
        drawnFigure = true;
    }
}

export function cleanPanel(): void {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    points.length = 0;

    perimeterResult.textContent = '0';
    squareResult.textContent = '0';
    drawnFigure = false;
    btnDrawCalc.disabled = false;
}

function calculateResult(): void {
    perimeterResult.textContent = String(calculatePerimeter(points));
    squareResult.textContent = String(calculateSquare(points));
}
