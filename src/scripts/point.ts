import {StatusAction} from './statusAction.js';

export type PointType = {
    x: number,
    y: number
};

export function isCorrectAmountOfPoints(status: StatusAction, points: PointType[]): boolean {
    const amount = points.length;
    let result: boolean = true;

    switch (status) {
        case StatusAction.NotEnoughPoints:
            return result = amount >= 3;
        case StatusAction.LimitPoints:
            return result = amount < 100;
    }
}
