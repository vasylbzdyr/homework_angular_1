import {StatusAction} from './statusAction.js';

export function showMessage(status: StatusAction): void {
    const statusCalculator = document.querySelector('.calculator__status-text') as HTMLParagraphElement;
    switch (status) {
        case StatusAction.NotEnoughPoints:
            statusCalculator.textContent = 'You need to choose 3 points';
            break;
        case StatusAction.LimitPoints:
            statusCalculator.textContent = 'You can choose only 100 points';
            break;
    }

    statusCalculator.classList.add('warning');

    setTimeout(() => {
        statusCalculator.textContent = "";
        statusCalculator.classList.remove('warning');
    }, 4000);
}
